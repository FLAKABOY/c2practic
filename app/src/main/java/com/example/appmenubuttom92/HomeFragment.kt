package com.example.appmenubuttom92

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeFragment : Fragment() {

    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Indica que este fragmento tiene un menú de opciones
        setHasOptionsMenu(true)
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Buscar y asignar la referencia del BottomNavigationView
        bottomNavigationView = view.findViewById(R.id.btnSocial);

        bottomNavigationView.menu.findItem(R.id.menu_facebook)?.setIcon(R.drawable.facebook)
        bottomNavigationView.menu.findItem(R.id.menu_gmail)?.setIcon(R.drawable.gmail)
        bottomNavigationView.menu.findItem(R.id.menu_instagram)?.setIcon(R.drawable.instagram)


        // Configurar el listener para los items del BottomNavigationView
        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.menu_facebook -> {
                    openUrl("https://www.facebook.com")
                    true
                }
                R.id.menu_gmail -> {
                    openUrl("https://mail.google.com/mail/u/0/#inbox")
                    true
                }
                R.id.menu_instagram -> {
                    openUrl("https://www.instagram.com")
                    true
                }
                else -> false
            }
        }

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_social, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun openUrl(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}
